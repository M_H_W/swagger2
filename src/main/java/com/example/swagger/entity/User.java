package com.example.swagger.entity;

/**
 * @类名: User
 * @描述: TODO
 * @作者: mengh
 * @创建时间: 2020/12/7
 * @版本: V1.0
 **/
public class User {

    private Long id;

    private String name;

    private Integer age;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
